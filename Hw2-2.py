import sys


def DPLL_Satisfiable(S):


	# Get the set of clauses and the set of Symbols

	clauses = []
	symbols =[]
	model = []
	
	#if empty list
	if len(S) == 1: 
		if isinstance(S[0],str): #case when S is a symbol
			clauses.append(S[0])
			symbols.append(S[0])
		elif S[0][0] == "not":       #case when S is like  [["not", symbol]]
			clauses.append(S[0])
			symbols.append(S[0][1])
		
	if len(S) == 2:
		 if S[0] == "not":       #case when S is like  ["not", symbol]
	 		clauses.append(S)
			symbols.append(S[1])
		
	elif S[0] == "or":      #case when S is 1 clause
		clauses.append(S)
		i = 1
		while i < len(S):
			if isinstance(S[i], str):  #case of a symbol
				symbols.append(S[i])
			elif isinstance(S[i], list):   #case of a literal of form ["not", symbol]
				symbols.append(S[i][1])
			i = i + 1	
		
	elif S[0] == "and":		# case when S is a conjunction of many clauses
		for s in S[1:len(S)]:
			clauses.append(s)
		i = 0
		while i < len(clauses):
			if isinstance(clauses[i],str): #case of a symbol
				symbols.append(clauses[i])
			elif isinstance(clauses[i], list):
				if clauses[i][0] == "not":   #case of a literal of form ["not", symbol]
					symbols.append(clauses[i][1])
				elif clauses[i][0] == "or": 	# case of  1 clause
					j = 1
					while j < len(clauses[i]):
						if isinstance(clauses[i][j], str):  #case of a symbol
							symbols.append(clauses[i][j])
						elif isinstance(clauses[i][j], list):   #case of a literal of form ["not", symbol]
							symbols.append(clauses[i][j][1])
						j = j + 1
			i = i + 1
		
	#get rid of duplicates in symbols

	i = 0 
	while i < len(symbols):
		j = i + 1
		while j < len(symbols):
			if symbols[i] == symbols[j]:
				del(symbols[j])
			else:
				j = j + 1
		i = i + 1	
		
	
	# get rid of inputs of the form ["or", A, ["not",A], .....] 
	# they dont affect satisfiability:(A or notA) = true
	# they will be dealt with later in the program
	i = 0
	flagi = 0
	while i < len(clauses):
		flagi = 0
		if len(clauses[i]) > 2 :
			if clauses[i][0] == "or":
				j = 1
				flagj = 0
				while (j <  len(clauses[i])):
					
					k = j + 1
					flagj = 0
					
					while (k  < len(clauses[i])):
						if ((isinstance(clauses[i][j],str)) and (len(clauses[i][k])>1)):
						  # case of  symbol
							if clauses[i][k][1] == clauses[i][j]: # found  not(symbol)
								del(clauses[i][k])
								del(clauses[i][j])
								flagj = 1	
								break
						elif isinstance(clauses[i][j],list): # case of not symbol
							if (isinstance(clauses[i][k],str) and (clauses[i][k] == clauses[i][j][1])):
							 # found that symbol
								del(clauses[i][k])
								del(clauses[i][j])
								flagj = 1
								break
								
						k = k + 1
					if flagj == 0:
						j = j + 1
			
				if len(clauses[i]) == 1:
					del(clauses[i])
					falgi =1
		if flagi == 0:
			i= i + 1	
			
			
	# call  DPLL 	
	if DPLL(clauses, symbols, model)== "true": 
		SAT = ["true"]
		for m in model:
			SAT.append(m)
			
	# assign truth values to the symbols that do not affect the Satisfiability
		s = 0 
		while(len(symbols)>0):
			mySym = symbols[s] +  "=" + "true"
			SAT.append(mySym)
			del(symbols[s])
		return SAT
	else:
		return ["false"]
	
def  FIND_PURE_SYMBOL(symbols, clauses):   
	
	i = 0
	while i < len(symbols):
		sym = 0
		j = 0  
		while j < len(clauses):
			if isinstance(clauses[j] , str): # clause is a symbol
				if clauses[j] == symbols[i]:
					# if symbol is  positive
					if sym == 2:
					# if this symbol  was  found in  negative 
					# then not pure
						sym = -1  
						break
					else:
						sym = 1       
			elif isinstance(clauses[j] , list):
				if clauses[j][0] == "not":  #  clause is like [not, symbol]
					if clauses[j][1] == symbols[i]:
					#  if symbol is negative
						if sym == 1:
					# if this symbol was earlier found positive 
					# then not pure
							sym = -1
							break
						else:
							sym = 2
				elif clauses[j][0] == "or":
					k = 1
					while k <len(clauses[j]):
						if isinstance(clauses[j][k] , str): # clauses[j][k] is symbol
							if clauses[j][k] == symbols[i]:
							# positive
								if sym == 2:
							# if found negative earlier
							# then not pure
									sym = -1
									break
								else:
									sym = 1
						elif isinstance(clauses[j][k] , list):
							if clauses[j][k][0] == "not": # clauses[j][k] is [not, symbol]
								if clauses[j][k][1] == symbols[i]:
								#negative
									if sym == 1:
								# if found positive earlier
								# then not pure
										sym = -1
										break
									else:
										sym = 2
						k = k + 1
			if sym == -1:
				break
			else:
				j = j + 1
		
		if sym == 1:      # positive pure symbol
			return [symbols[i], "true"]
		elif sym == 2:	 # negative pure symbol
			return [symbols[i], "false"]
		
		i = i + 1	
		
	#if  no Pure Symbol was found
	return "null"

def FIND_UNIT_CLAUSE(symbols, clauses):

	i = 0
	while i < len(clauses):
		if isinstance(clauses[i], str):  
		# positive  unit  clause
			return [clauses[i],"true"]
		elif ((isinstance(clauses[i], list)) and (clauses[i][0] == "not")):
		# negative unit clause
			return [clauses[i][1],"false"]
		else:
			i = i + 1
	# if no unit  clause was  founded
	return "null"



# Davis–Putnam–Logemann–Loveland (DPLL) Algorithm
def DPLL(clauses, symbols, model):
	

	
	if len(clauses) == 0:  # Empty Sentence
		return "true"
	else :
		i = 0
		while (i<len(clauses)):         
			if len(clauses[i]) == 0:   # Empty Clause
				return "false"
			i = i + 1
	
# Apply Pure symbol rule if applicable
	P = FIND_PURE_SYMBOL(symbols, clauses)
	
	if P != "null":    # If Pure Symbol FOund
	
	# remove the pure symbol from the symbols list
		k = 0 
		while k < len(symbols):
			if symbols[k] == P[0]:
				del(symbols[k])
				break
			else:
				k = k + 1
		
		# add the pure symbol and its truth value to the model 
		mys = P[0] + "=" + P[1]
		model.append(mys)
		
		
		# remove all the clauses containing the pure symbol 
		i = 0 
		flag =0
		while i < len(clauses):
			flag =0
			if len(clauses[i]) == 0 :
					i = i + 1
					continue
			elif ( (isinstance(clauses[i], str)) and (P[1] == "true") and (clauses[i] == P[0])):  
			# clause is a symbol and that symbol is the pure symbol
			 	del(clauses[i])
			 	flag = 1 
			elif((clauses[i][0] == "not") and  (P[1] == "false") and (clauses[i][1] == P[0])): 
			# clause is a "not" clause and that clause is the not of the pure symbol
				del(clauses[i])
				flag =1
			elif clauses[i][0] == "or":
			#clause is an or  clause
				j=1 
				
				while j < len(clauses[i]):
					if ((isinstance(clauses[i][j], str)) and (P[1] == "true") and (clauses[i][j] == P[0])):
			# symbol case and that symbol is the pure symbol
			 			del(clauses[i])
			 			flag = 1 
			 			break
			 		
			 		elif ((clauses[i][j][0] == "not") and  (P[1] == "false") and (clauses[i][j][1] == P[0])):
			# clause is a "not" clause and that clause is the not of the pure symbol
			 			del(clauses[i])
			 			flag = 1
			 			break
			 			
			 		else: 
			 			j = j + 1	
			 	
				if i < 	len(clauses):	
						if ((clauses[i][0] == "or") and (len(clauses[i]) == 1)): #  only "or" left
			 				clauses[i] = []
			 			elif  ((clauses[i][0] == "or") and (len(clauses[i]) == 2)):  #  only ["or",symbol] left
							clauses[i] = clauses[i][1]
					
			if flag == 0: # clause doesn't contain the pure symbol
				i = i + 1 
	
		# call  DPLL after  removing the pure symbol
		return DPLL(clauses, symbols, model)
	
	else: 	
	# if no Pure Symbol was found
	
		# apply unit clause rule if  applicable
		U = FIND_UNIT_CLAUSE(symbols, clauses)
		
		UnitClause = 0
		if not ( U == "null"):
			UnitClause = 1
			
			# remove the unit clause symbol from the symbols list
			k = 0 
			while k < len(symbols):
				if symbols[k] == U[0]:
					del(symbols[k])
					break
				else:
					k = k + 1
		
			# add the unit clause symbol and its truth value to the model 
		 
			mys = U[0] + "=" + U[1]
			model.append(mys)
			
			
			# remove all the clauses containing the unit clause 
			# including the unit clause itself
			# and  the complementary of the unit  clause in other clasues
			i = 0 
			flag =0
			while i < len(clauses): 
				flag = 0
				if len(clauses[i]) == 0 :
					i = i + 1
					continue
				
				elif( (isinstance(clauses[i], str)) and (U[1] == "true") and (clauses[i] == U[0])):  
				# clause is a symbol and that symbol is the unit clause
				 	del(clauses[i])
				 	flag = 1     # flagging to not increment i
				elif ((clauses[i][0] == "not") and  (U[1] == "false") and (clauses[i][1] == U[0])): 
				# clause is a "not" clause and that clause is the unit clause
				 	del(clauses[i])
				 	flag =1
				elif ((clauses[i][0] == "not") and  (U[1] == "true") and (clauses[i][1] == U[0])): 
				# Complementary case
				 	clauses[i]=[] # gives an empty clause
				elif clauses[i][0] == "or":
				#clause is an or clause
					j=1 
					flagj = 0 
					while j < len(clauses[i]):
						flagj = 0 
						if ((isinstance(clauses[i][j], str)) and (U[1] == "true") and (clauses[i][j] == U[0])):
				# symbol case and that symbol is the unit clause
			 				del(clauses[i])
			 				flag = 1 # flagging to not increment i
			 				break
			 				
			 			elif ((clauses[i][j][0] == "not") and  (U[1] == "false") and (clauses[i][j][1] == U[0])):
				# clause is a "not" clause and that clause is the unit clause
			 				del(clauses[i])
			 				flag = 1
			 				break
			 			elif ((clauses[i][j][0] == "not") and  (U[1] == "true") and (clauses[i][j][1] == U[0])):
				# Complementary case
			 				del(clauses[i][j]) # delete the complementary
							flagj =1   # flagging to not increment j
							break
			 			if flagj == 0: 
			 				j = j + 1		
					if i < 	len(clauses):	
						if ((clauses[i][0] == "or") and (len(clauses[i]) == 1)): #  only "or" left
			 				clauses[i] = []
			 			elif ((clauses[i][0] == "or") and (len(clauses[i]) == 2)): #  only ["or",symbol] left
							clauses[i] = clauses[i][1]
				if flag == 0: # clause is not the unit clause or it does not contain it or its complementary
					i = i + 1 
		
			# call  DPLL after after applying the unit clause rule	
			return DPLL(clauses, symbols, model)
	
	#in case there were no Unit clauses 	
		else:
	# apply  the Splitting rule
			
			
			#Guess First symbol is True
			
			First = symbols[0]
			Rest =  symbols[1:len(symbols)]
			modelTrue= [] 
			clausesTrue = []
			
			for m in model:
				modelTrue.append(m)
			for c in clauses:
				clausesTrue.append(c)
			
			# Assign true to First
			myFirst = [First,"true"]
			mys = myFirst[0] + "=" + myFirst[1]
			modelTrue.append(mys)
			
			
			# remove all the clausesTrue containing the First symbol and its complementaries in other clauses
			i = 0 
			flag =0
			while i < len(clausesTrue):
				flag = 0
				if len(clausesTrue[i]) == 0 :
					i = i + 1
					continue
				
				elif ( (isinstance(clausesTrue[i], str))  and (clausesTrue[i] == myFirst[0])):  
				# clause is a symbol and that symbol is First
				 	del(clausesTrue[i])
				 	flag = 1
				elif ((clausesTrue[i][0] == "not") and (clausesTrue[i][1] == myFirst[0])): 
				# Complementary case
				 	clausesTrue[i]=[] # gives an empty clause
				elif clausesTrue[i][0] == "or":
				#clause is an or  clause
					j=1 
					flagj=0
					while j < len(clausesTrue[i]):
						flagj=0
						if ((isinstance(clausesTrue[i][j], str)) and (clausesTrue[i][j] == myFirst[0])):
				# symbol case and that symbol is First 
				 			del(clausesTrue[i])
				 			flag = 1  # flag to no  increment i  
				 			break
				 		elif ((clausesTrue[i][j][0] == "not") and (clausesTrue[i][j][1] == myFirst[0])):
				# Complementary case
				 			del(clausesTrue[i][j])
				 			flagj = 1 # flag to no  increment j
							break
				 		if flagj == 0: 
				 			j = j + 1		
					if i < 	len(clausesTrue):	
						if ((clausesTrue[i][0] == "or") and (len(clausesTrue[i]) == 1)): #  only "or" left
			 				clausesTrue[i] = []
			 			elif ((clausesTrue[i][0] == "or") and(len(clausesTrue[i]) == 2)): #  only ["or",symbol] left
							clausesTrue[i] = clausesTrue[i][1]
				if flag == 0: # clause is not First or doesn't contain it or its complementary
					i = i + 1
					
			
			#call  DPLL after the guessing True
			if DPLL(clausesTrue, Rest, modelTrue) == "true":
			# if it returns true then sentence is satisfiable for First = true so return true 
				return "true"
			
		# in case guessing true didn't lead to satisfiability
			else:
				
				#Guess First is False
				
				First = symbols[0]
				Rest =  symbols[1:len(symbols)]
				modelFalse= [] 
				clausesFalse = []
				
				for m in model:
					modelFalse.append(m)
				for c in clauses:
					clausesFalse.append(c)
				
				
				myFirst = [First,"false"]
				mys = myFirst[0] + "=" + myFirst[1]
				modelFalse.append(mys)
				
				
				# remove all the clausesFalse containing the First symbol and its complementaries
				i = 0 
				flag =0
				while i < len(clausesFalse):
				
					flag = 0
					if len(clausesFalse[i]) == 0 :
						i = i + 1
						continue
					
					elif ( (isinstance(clausesFalse[i], str)) and (clausesFalse[i] == myFirst[0])):  
					# Complementary case
					 	clausesFalse[i]=[] # gives an empty clause
					elif ((clausesFalse[i][0] == "not") and (clausesFalse[i][1] == myFirst[0])): 
					# same literal
					 	del(clausesFalse[i])
					 	flag =1 # flagging to not  increment i
					elif clausesFalse[i][0] == "or":
					#clause is an or  clause
						j=1 
						flagj = 0 
						while j < len(clausesFalse[i]):
							flagj = 0 
							if ((isinstance(clausesFalse[i][j], str)) and (clausesFalse[i][j] == myFirst[0])):
					# Complementary case
					 			del(clausesFalse[i][j])
					 			flagj = 1 #  flag to not increment j 
					 			break
					 		elif ((clausesFalse[i][j][0] == "not") and (clausesFalse[i][j][1] == myFirst[0])):
					# clause is a "not" clause and is First
					 			del(clausesFalse[i])
					 			flag = 1
					 			break
						 	
							if flagj == 0: 
					 			j = j + 1
					 	if i < 	len(clausesFalse):	
							if len(clausesFalse[i]) == 1: #  only "or" left
				 				clausesFalse[i] = []
				 			elif len(clausesFalse[i]) == 2: #  only ["or",symbol] left
								clausesFalse[i] = clausesFalse[i][1]
					if flag == 0: 
					# clause is not First or doesn't contain it or its complementary
						i = i + 1
				
				#call  DPLL after Guessing False
				if DPLL(clausesFalse, Rest, modelFalse) == "true":
				# if it returns true then sentence is satisfiable for First = false so return true 
					return "true"
		
				else:
				
				# Sentence  is not  Satisfiable
					return "false"



#read  Sentenses in CNF form form  file 
inputFile = open(sys.argv[2]).readlines()
outputFile = open("CNF_satisfiability.txt", 'w')
inputFile.pop(0)


# For each sentence run Davis–Putnam–Logemann–Loveland (DPLL) algorithm 
# to detemine if sentence is satisfiable or not, and if it is satisfiable,
# determines the values that satisfy it.

for line in inputFile:
	S = eval(line)
	SAT = DPLL_Satisfiable(S)
	outputFile.write(str(SAT) + '\n')
outputFile.close()
