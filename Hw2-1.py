import sys
 
   
# Eiminate bidirectional 

def BidirElimination(SL):
	if isinstance(SL, str):
		return SL
	elif [isinstance(SL, list)]: 	
		if SL[0] == 'iif':
			SL1 =  SL[1]
			SL2 =  SL[2]
			SL[0] = "and"
			SL[1] = ["implies", SL1, SL2]  
			SL[2] = ["implies", SL2, SL1]
			SL[1][1] = BidirElimination(SL[1][1])
			SL[1][2] = BidirElimination(SL[1][2])
		else:
			i = 1 
			while i<len(SL):
				SL[i] = BidirElimination(SL[i])
				i = i + 1		
	
	return SL

# Eliminate Implications 
def ImplElimination(SL):
	
	if isinstance(SL, str):
		return SL
	elif [isinstance(SL, list)]: 	
		if SL[0] == 'implies':
				SL1 =  SL[1]
				SL[0] = "or"
				SL[1] = ["not", SL1]
				SL[1][1] = ImplElimination(SL[1][1])
			#SL[2]= ImplElimination(SL[2])
		else:
			i = 1 
			while i<len(SL):
				SL[i] = ImplElimination(SL[i])
				i = i + 1
	return SL


# Apply Deorgan laws to switch Sentence to a conjunction
def DeMorgan(SL):
	
	if isinstance(SL, str):
		return SL
	
	elif SL[0] == 'not':
		SL1= SL[1] 
		if not isinstance(SL1, str):
			if(SL1[0] == "or"):		      # If a negation of a Disjunction
				SL[0] = "and"		      # switch to conjunction of negations
				del(SL[1])		     	  #(e.g not(A or B) to (not A)and(not B))
				for S in SL1[1:len(SL1)]:
					S = ["not", S]
					SL.append(S)
					
			elif(SL1[0] == "and"):	      # If a negation of a conjunction
				SL[0] = "or"	       	  # switch to disjunction of negations
				del(SL[1])			      #(e.g not(A and B) to (not A)or(not B))
				for S in SL1[1:len(SL1)]:
					S = ["not", S]
					SL.append(S)
			
			elif(SL1[0] == "not"):
				SL =SL1[1]
				SL = DeMorgan(SL)
						
				
							
			i = 1 
			while i<len(SL):
				SL[i] = DeMorgan(SL[i])
				i = i + 1		
				
		
	else:
		i = 1 
		while i<len(SL):
			SL[i] = DeMorgan(SL[i])
			i = i + 1	
									 
	return SL
	
	
# apply distributivity laws

def Distributivity(SL):
	
	if isinstance(SL, str):
		return SL
	if isinstance(SL, list): 	
		if (len(SL) > 2):
			#print "inside first if\n", SL , '\n'
			S=[]
			count = 0
			if (SL[0] == 'or'):
				i=1						#put all the conjunction on the left
				j=1
				#print "inside second if\n", SL , '\n'
				while j<(len(SL)):
					if isinstance(SL[j], list): 
						if SL[j][0] == "and":
							s = SL[j]
							SL[j] = SL[i]
							SL[i] = s
							i = i + 1
							count = count + 1  	 # count the number of conjunctions
					j = j + 1
				#print "After the first while \n", SL , '\n'
			if count > 0 :		      # if there are  any conjunctions
		
				while(count > 1):      # apply Distributivity on the conjuctions
					SL1 = SL[1]
					SL2 = SL[2]
					S = ["and"]
					for s1 in SL1[1:len(SL1)]:
						S.append(["or", s1, SL2])
						#print S
					SL[1] =  S
					del(SL[2])
					Distributivity(SL[1])
					count = count - 1 
	

				i = len(SL) - 1  #apply Distributivity on the result and the rest of Expressions
				while (i > 1):
					SL1 = SL[1]
					SL2 = SL[2]
					S = ["and"]
			
					for s1 in SL1[1:len(SL1)]:
						S.append(["or", s1, SL2])
					SL[1] =  S
					del(SL[2])
					Distributivity(SL[1])
					i = i - 1
			
				SL = S
			else:
				k = 1 
				while k<len(SL):
					SL[k] = Distributivity(SL[k])
					k = k + 1		
	return SL
	
# Apply associativity law  (AND)
	
def AssociativityAND(SL):
	
	
	if isinstance(SL, str):
		return SL
		
	elif isinstance(SL, list):
		if(SL[0] == 'and'):
			i=1
			while (i < len(SL)) :
				while( (isinstance(SL[i], list)) and (SL[i][0] == "and") ):
					for S in SL[i][1:len(SL[i])] :
						SL.append(S)
					del(SL[i])
				i = i + 1
		else:
			j = 1 
			while (j < len(SL)) :
				SL[j] = AssociativityAND(SL[j])
				j = j + 1
	return SL
	
# Apply associativity law  (OR)	

def AssociativityOR(SL):
	
	if isinstance(SL, str):
		return SL
		
	elif isinstance(SL, list):
		if(SL[0] == 'or'):
			i=1
			while (i < len(SL)) :
				while( (isinstance(SL[i], list)) and (SL[i][0] == "or") ):
					for S in SL[i][1:len(SL[i])] :
						SL.append(S)
					del(SL[i])
				i = i + 1
		else:
			j = 1 
			while (j < len(SL)) :
				SL[j] = AssociativityOR(SL[j])
				j = j + 1
	return SL

# Factorization

def Factor(SL):

	if isinstance(SL,str):
		return SL
	
	if SL[0] == "not":       # case of 1 literal
		return SL
		
	if SL[0] == "or":		# case of 1 Clause
		i=1
		while i < len(SL):
			j =  i + 1 
			if isinstance(SL[i], str):    # case  of  symbol
				while  j < len(SL):
					if ( (isinstance(SL[j],str)) and (SL[j] == SL[i]) ):
						del(SL[j])
					else: 
						j = j + 1
			elif isinstance(SL[i], list):    # case of ["not", Symbol]
				while  (j < len(SL) and isinstance(SL[i], list)): 
					if ( (isinstance(SL[j],list)) and (SL[j][1]==SL[i][1]) ):
						del(SL[j])
					else:
						j = j + 1
			i = i + 1					
		
	if SL[0] == "and":
		i = 1 
		while i < len(SL):			
			if (isinstance(SL[i], list) and (len(SL[i])> 0)):
				if SL[i][0] == "or":
					SL[i] = Factor(SL[i])				
			i = i + 1
		
		i = 1
		while i < len(SL):
			j = i + 1 
			if isinstance(SL[i], str):    # case  of  symbol
				while  j < len(SL):
					if ( (isinstance(SL[j],str)) and (SL[j] == SL[i]) ):
						del(SL[j])
					else: 
						j = j + 1
			elif ((isinstance(SL[i], list)) and (SL[i][0] == "not")):    # case of ["not", Symbol]
				while  j < len[SL]:
					if ( (isinstance(SL[j],list)) and ((SL[j][0] == "not")) and (SL[j][1]==SL[i][1]) ):
						del(SL[j])
					else:
						j = j + 1
			elif ((isinstance(SL[i], list)) and (SL[i][0] == "or")):    # case of a disjunction
				while  j < len(SL):
					equal = 0
					if ((isinstance(SL[j], list)) and (SL[j][0] == "or")):
						if len(SL[j]) == len(SL[i]):
							ii = 1
							equal = 0
							while(ii < len(SL[i])):
								jj = 1
								while(jj< len(SL[j])):
									if SL[i][ii] == SL[j][jj]:
										equal += 1	
									jj = jj + 1
								ii = ii + 1
					if equal == len(SL[j]) - 1:
						del(SL[j])
					else:
						j = j + 1
			i = i + 1
				
				
		if len(SL) == 2:
			SL = SL[1]
		elif len(SL) == 1:
			SL = []
			print "Empty Sentence"
	return SL	
	
	
def CTCNF(SL):
	
	# run the elimination in the correct order on the sentence
	
	SL = BidirElimination(SL)
	SL = ImplElimination(SL)
	SL = DeMorgan(SL)
	SL = Distributivity(SL) 
	SL = AssociativityAND(SL)
	SL = AssociativityOR(SL)
	Sl = Factor(SL)
	
	return SL
	
# read sentences from file	
inputFile = open(sys.argv[2]).readlines()
outputFile = open("sentences_CNF", 'w')
inputFile.pop(0)

# Transform each sentence to its CNF 
for line in inputFile:
	SL = eval(line)
	SL = CTCNF(SL)
	outputFile.write(str(SL) + '\n')
outputFile.close()
